const webpack = require('webpack')
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin')
const ExtractTheme1 = new ExtractTextWebpackPlugin('theme1.css')
const ExtractTheme2 = new ExtractTextWebpackPlugin('theme2.css')
const postcssCustomProperties = require('postcss-custom-properties')
const IgnoreEmitPlugin = require('ignore-emit-webpack-plugin')
const AssetsPlugin = require('assets-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const WebpackShellPlugin = require('webpack-shell-plugin')
// const WriteFilePlugin = require('write-file-webpack-plugin')

const assetsPlugin = new AssetsPlugin({
    filename: 'assets.json',
    prettyPrint: true,
    path: path.join(__dirname, 'dist')
})

const customProps1 = postcssCustomProperties({
    preserve: false
})

const customProps2 = postcssCustomProperties({
    preserve: false
})

customProps1.setVariables({
    '--body-bgColor': 'orange',
    '--App-color': 'maroon'
})

customProps2.setVariables({
    '--body-bgColor': 'lime',
    '--App-color': 'teal'
})

const jsModule = {
    entry: {
        vendor: ['lodash' ],
        app: ['babel-polyfill', 'whatwg-fetch', './src/main.js']
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: 'ignore-loader' // ignore all css
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        css: 'ignore-loader', // ignore all css
                        js: 'babel-loader'
                    }
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            }
        ]
    }
}

const theme1module = {
    entry: {
        theme1: './src/main.js'
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTheme1.extract({
                    use: [
                        'css-loader',
                        { loader: 'postcss-loader', options: { plugins: [ customProps1 ] } }
                    ]
                })
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        css: ExtractTheme1.extract({
                            use: 'css-loader'
                        })
                    },
                    postcss: [ customProps1 ]
                }
            }
        ]
    }
}

const theme2module = {
    entry: {
        theme2: './src/main.js'
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTheme2.extract({
                    use: [
                        'css-loader',
                        { loader: 'postcss-loader', options: { plugins: [ customProps2 ] } }
                    ]
                })
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        css: ExtractTheme2.extract({
                            use: 'css-loader'
                        })
                    },
                    postcss: [ customProps2 ]
                }
            }
        ]
    }
}

module.exports = [
    {
        ...jsModule,
        plugins: [
            new HtmlWebpackPlugin({
                inject: false,
                template: require('html-webpack-template'),
                appMountId: 'app',
                title: 'foobar',
                filename: 'index.html'
            }),
            new webpack.optimize.CommonsChunkPlugin({
                name: 'vendor',
                minChunks: Infinity
            }),
            assetsPlugin,
            new WebpackShellPlugin({
                onBuildStart: ['echo "Starting"'],
                onBuildEnd: ['echo "Ending"', 'node postbuild.js']
            }),
            // new WriteFilePlugin()
        ]
    },
    {
        ...theme1module,
        plugins: [
            ExtractTheme1,
            new IgnoreEmitPlugin(/theme1\.js$/),
            assetsPlugin
        ]
    },
    {
        ...theme2module,
        plugins: [
            ExtractTheme2,
            new IgnoreEmitPlugin(/theme2\.js$/),
            assetsPlugin
        ]
    }
]
