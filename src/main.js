import Vue from 'vue'
import './style.css'
import App from './App.vue'

export default new Vue({
    render: h => h(App),
    components: { App }
}).$mount('#app')


