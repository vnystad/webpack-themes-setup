const fs = require('fs')
const { promisify } = require('util')
const readFile = promisify(fs.readFile)
const writeFile = promisify(fs.writeFile)

const path = require('path')
const dom = require('minidom')

module.exports = (async function() {

    const filePath = path.join('./dist/index.html')
    const assetsPath = path.join('./dist/assets.json')

    const htmlFile = await readFile(filePath, 'utf8')
    const assetsFile = await readFile(assetsPath, 'utf8')

    const document = dom(htmlFile)
    const link = document.createElement('link')

    link.setAttribute('rel', 'stylesheet')
    link.setAttribute('href', JSON.parse(assetsFile).theme1.css)
    link.setAttribute('data-theme', 'theme1')

    const script = document.createElement('script')
    script.textContent = `window.STATIC_ASSETS = ${assetsFile}`

    document.getElementsByTagName('head')[0].appendChild(link)
    document.getElementsByTagName('head')[0].appendChild(script)

    await writeFile(filePath, document.outerHTML, 'utf8')

    console.log('done', document.outerHTML)

}())
