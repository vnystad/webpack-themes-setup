const webpack = require('webpack')
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin')
const postcssCustomProperties = require('postcss-custom-properties')
const IgnoreEmitPlugin = require('ignore-emit-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')


const ExtractTheme1 = new ExtractTextWebpackPlugin('theme1.css')
const ExtractTheme2 = new ExtractTextWebpackPlugin('theme2.css')

const customProps1 = postcssCustomProperties({
    preserve: false
})

const customProps2 = postcssCustomProperties({
    preserve: false
})

customProps1.setVariables({
    '--body-bgColor': 'orange',
    '--App-color': 'maroon'
})

customProps2.setVariables({
    '--body-bgColor': 'lime',
    '--App-color': 'teal'
})

const themes = {
    theme1: {
        '--body-bgColor': 'orange',
        '--App-color': 'maroon'
    },
    theme2: {
        '--body-bgColor': 'lime',
        '--App-color': 'teal'
    },
    theme3: {
        '--body-bgColor': 'deeppink',
        '--App-color': 'violet'
    }
}

const configs = themes.map( (theme, i) => {

    const entry = {}

    if (i > 0) {
        entry.useless = ['./src/main.js']
    } else {
        entry.vendor = ['lodash']
        entry.app = ['./src/main.js']
    }

    const output = {
        filename: '[name].js',
        path: path.join(__dirname, 'dist')
    }

    const module = {}
    const rules = module.rules = []

    const ExtractTheme = new ExtractTextWebpackPlugin(`theme${i}.css`)

    const customProps = postcssCustomProperties({
        preserve: false
    })

    customProps.setVariables(theme)

    const css = {
        test: /\.css$/,
        use: ExtractTheme.extract({
            use: [
                'css-loader',
                { loader: 'postcss-loader', options: { plugins: [ customProps ] } }
            ]
        })
    }

    rules.push(css)

    return {
        ...entry,
        ...output,
        ...module
    }
})

const theme1module = {
    entry: {
        vendor: ['lodash'],
        app: ['./src/main.js']
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTheme1.extract({
                    use: [
                        'css-loader',
                        { loader: 'postcss-loader', options: { plugins: [ customProps1 ] } }
                    ]
                })
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        css: ExtractTheme1.extract({
                            use: 'css-loader'
                        }),
                        js: 'babel-loader'
                    },
                    postcss: [ customProps1 ]
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            }
        ]
    }
}

const theme2module = {
    entry: {
        useless: './src/main.js'
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTheme2.extract({
                    use: [
                        'css-loader',
                        { loader: 'postcss-loader', options: { plugins: [ customProps2 ] } }
                    ]
                })
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        css: ExtractTheme2.extract({
                            use: 'css-loader'
                        }),
                        js: 'ignore-loader' // ignore lodash etc.
                    },
                    postcss: [ customProps2 ]
                }
            }
        ]
    }
}

module.exports = [
    {
        ...theme1module,
        plugins: [
            ExtractTheme1,
            new HtmlWebpackPlugin({
                inject: false,
                template: require('html-webpack-template'),
                appMountId: 'app',
                title: 'foobar',
                filename: 'index.html'
            }),
            new webpack.optimize.CommonsChunkPlugin({
                name: 'vendor',
                minChunks: Infinity
            })
        ]
    },
    {
        ...theme2module,
        plugins: [
            ExtractTheme2,
            new IgnoreEmitPlugin(/useless\.js$/)
        ]
    }
]
